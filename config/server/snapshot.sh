#!/bin/sh

# Create a timestamp
date=`date "+%Y-%m-%dT%H_%M_%S"`

# Folder to snapshot on local machine
SOURCE=/Library/WebServer/Documents/snapshots

# Destination folder on the Server
DEST=/var/www/snapshots

# Execute rsync followed by cleanup
rsync -azvP \
  --delete \
  --link-dest=../current \
  $SOURCE sysuser@51.91.76.207:$DEST/backup-$date \
  && ssh sysuser@51.91.76.207 \
  "rm -rf $DEST/current \
  && ln -s $DEST/backup-$date $DEST/current"