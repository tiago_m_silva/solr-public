<?php
/**
* Plugin Name: Solr API
* Description: Plugin that provides all the information to the Website
* Version: 1.0
* Author: Load Interactive
* Author URI: https://load.digital
*/

require_once('base.php');

$api = 'solr/v1';
add_action('rest_api_init', function() use ($api)
{
	register_rest_route($api, 'utils',
	[
    	'methods'  => 'POST',
		'callback' => 'Utils::importJsonData'
    ]);

    register_rest_route($api, 'person',
	[
    	'methods'  => 'GET',
		'callback' => 'PeopleController::getAll'
    ]);

    register_rest_route($api, 'person/(?P<id>[\d]+)',
	[
    	'methods'  => 'GET',
		'callback' => 'PeopleController::get'
    ]);
});