<?php
class Utils
{
    public static function importJsonData()
    {
        $json = json_decode(file_get_contents(__DIR__ . '/data.json'), TRUE); // 9999 objects
        foreach ($json as $person)
        {
            if ($person['id'] < 2853)
            {
                continue;
            }
            set_time_limit(0);
            Person::create($person);
        }
    }
}