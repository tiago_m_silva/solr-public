<?php
class Person
{
    public static function getAll()
    {
        return get_posts(
        [
            'fields'      => 'ids',
            'post_type'   => 'person',
            'post_status' => 'publish',
            'numberposts' => -1
        ]);
    }

    public static function get($id)
    {
        $tags = array_map(function($tag){ return trim($tag['name']); }, get_field('tags', $id));
        return
        [
            'id'         => (int) $id,
            'name'       => get_the_title($id),
            'isActive'   => get_field('isActive', $id),
            'gender'     => trim(get_field('gender', $id)),
            'address'    => trim(get_field('address', $id)),
            'about'      => trim(get_field('about', $id)),
            'registered' => trim(get_field('registered', $id)),
            'latitude'   => (float) get_field('latitude', $id),
            'longitude'  => (float) get_field('longitude', $id),
            'tags'       => $tags
        ];
    }

    public static function create($person)
    {
        $id = wp_insert_post(
        [
            'post_title'  => $person['name'],
            'post_type'   => 'person',
            'post_status' => 'publish'
        ]);

        if (is_wp_error($id))
        {
            return FALSE;
        }

        foreach ($person as $key => $value)
        {
            if ($key === 'id')
            {
                continue;
            }

            if ($key === 'tags')
            {
                foreach ($value as $tag)
                {
                    add_row('tags', ['name' => $tag], $id);
                }
                continue;
            }

            update_field($key, $value, $id);
        }
        return TRUE;
    }
}