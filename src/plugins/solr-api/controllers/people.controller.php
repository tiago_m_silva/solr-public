<?php
class PeopleController
{
    public static function getAll()
    {
        return array_map(function($id){ return Person::get($id); }, Person::getAll());
    }

    public static function get($request)
    {
        return Person::get($request['id']);
    }
}