## To get the list of the existing containers
- `docker ps -a`

## To start
- `docker-compose up --build -d`

## To stop
- `docker-compose stop`

## To stop and remove volumes - BE CAREFUL with this - it will remove all containers
- `docker-compose down`

## to get a bash shell in the container
- `docker exec -it CONTAINER_NAME /bin/bash`

## set permissions on uploads and plugins
- `chown -R www-data:www-data /var/www/html/wp-content/uploads`
- `chown -R www-data:www-data /var/www/html/wp-content/plugins`

## export database
- `docker exec -i CONTAINER_NAME mysqldump -uDB_USER -pDB_PASS DB_NAME > dump_DATE.sql`

## import database
- `docker exec -i CONTAINER_NAME mysql -uDB_USER -pDB_PASS DB_NAME < dump_DATE.sql`

## copy file from source to container
- `docker cp FILENAME.zip CONTAINER_NAME:/path/of/folder/to/be/copied`

## Useful command - Extract Zip file inside container
- Get in the container, via bash shell
- `apt-get update && apt-get install zip -y`
- Now you're able to extract the zip file by `unzip FILENAME.zip`